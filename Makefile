all: clean firefox chrome

.PHONY: chrome

chrome:
	rm -rf ./chrome/plugin/
	mkdir -p ./chrome/plugin/
	cp ./chrome/manifest.json ./chrome/plugin/
	cp -r ./resources/* ./chrome/plugin/

.PHONY: firefox

firefox:
	rm -rf ./firefox/plugin/
	mkdir -p ./firefox/plugin/
	cp ./firefox/chrome.manifest ./firefox/plugin/
	cp ./firefox/install.rdf ./firefox/plugin/
	cp -r ./firefox/resources ./firefox/plugin/
	cp -r ./resources/* ./firefox/plugin/resources/

	mkdir ./firefox/plugin/chrome
	cp -r ./firefox/content ./firefox/plugin/chrome/
	(cd ./firefox/plugin/; rm ../miroit.xpi; zip -r ../miroit.xpi *)

ifirefox:
	mkdir -p ~/Library/Application\ Support/Mozilla/Extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/
	cp firefox/miroit.xpi ~/Library/Application\ Support/Mozilla/Extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/{79287D2F-D399-471A-A95E-BCBED9AEDB3B}.xpi

ifirefoxlinux:
	mkdir -p ~/.mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/
	cp firefox/miroit.xpi ~/.mozilla/extensions/{ec8030f7-c20a-464f-9b0e-13a3a9e97384}/{79287D2F-D399-471A-A95E-BCBED9AEDB3B}.xpi

open:
	open /Library/Internet\ Plug-Ins/
	open /Library/Application\ Support/Mozilla/
	open ~/Library/Internet\ Plug-Ins/
	open ~/Library/Application\ Support/Mozilla/

clean:
	rm -rf ./firefox/plugin/
	rm -rf ./chrome/plugin/
