MiroIt.System = {

  browser : null,
  os : "Unknown OS",

  init : function() {
    // Mozilla/5.0 (Windows NT 5.1; rv:9.0.1) Gecko/20100101 Firefox/9.0.1
    if (navigator.userAgent.indexOf("Windows NT 5.1") != -1)
      os = "WINXP";
    // Mozilla/5.0 (Windows NT 6.1; WOW64; rv:9.0.1) Gecko/20100101 Firefox/9.0.1
    if (navigator.userAgent.indexOf("Windows NT 6.1") != -1)
      os = "WIN7";
    if (navigator.appVersion.indexOf("Mac") != -1)
      os = "MACOS";
    if (navigator.appVersion.indexOf("X11") != -1)
      os = "UNIX";
    if (navigator.appVersion.indexOf("Linux") != -1)
      os = "LINUX";
  },

  invokeMiro : function(uri) {
    var cmd;
    var args;

    switch (os) {
    case 'WINXP':
      cmd = 'C:\\Program Files\\Participatory Culture Foundation\\Miro\\Miro.exe';
      args = [ uri ];
      break;
    case 'WIN7':
      cmd = 'C:\\Program Files (x86)\\Participatory Culture Foundation\\Miro\\Miro.exe';
      args = [ uri ];
      break;
    case 'MACOS':
      cmd = '/usr/bin/open';
      args = [ '-g', '-a', '/Applications/Miro.app', uri ];
      break;
    case 'UNIX':
    case 'LINUX':
      cmd = '/usr/bin/miro';
      args = [ uri ];
      break;
    }

    var execFile = Components.classes["@mozilla.org/file/local;1"].createInstance(Components.interfaces.nsILocalFile);
    var process = Components.classes["@mozilla.org/process/util;1"].createInstance(Components.interfaces.nsIProcess);
    execFile.initWithPath(cmd);
    process.init(execFile);
    process.run(false, args, args.length);
  }

};
