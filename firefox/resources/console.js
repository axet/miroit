MiroIt.Console = {

  con : null,

  init : function() {
    this.con = Components.classes["@mozilla.org/consoleservice;1"].getService(Components.interfaces.nsIConsoleService);
  },

  log : function(e) {
    this.con.logStringMessage(e);
  },

  debug : function(e) {
  }

};
