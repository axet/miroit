window.addEventListener("load", function() {
  MiroIt.System.init();
  MiroIt.Console.init();
  MiroIt.Browser.init();
}, false);

window.addEventListener("unload", function() {
  MiroIt.Browser.uninit();
}, false);
