var MiroItService = {};

MiroItService.IPC = {

  send : function(uri) {
    var obj = document.createElement("div");
    obj.setAttribute("type", "application/miroit-run-plugin");
    obj.setAttribute("uri", uri);
    document.body.appendChild(obj);
    document.body.removeChild(obj);
  }

};
