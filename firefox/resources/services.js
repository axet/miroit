MiroIt.Services = {

  googlereader : {
    probe : function(doc) {
      var chrome = doc.getElementById('chrome');
      if (chrome == null)
        return false;

      return true;
    },
    url : 'resource://miroit/services/googlereader.js'
  },

  probe : function(doc) {
    var services = [ this.googlereader ];

    for ( var i = 0; i < services.length; i++) {
      var service = services[i];
      if (service.probe(doc)) {
        var invokeIPC = doc.getElementById('invokeIPC');
        if (invokeIPC == null) {
          invokeIPC = this.inject(doc, 'invokeIPC', 'resource://miroit/ipc.js');
        }
        var invokeService = doc.getElementById('invokeService');
        if (invokeService == null) {
          invokeService = this.inject(doc, 'invokeService', service.url);

          var that = this;
          doc.body.addEventListener('DOMNodeInserted', entriesListener = function(event) {
            that.catchInvokeMiro(doc, event);
          }, false);

        }
        return true;
      }
    }

    return false;
  },

  inject : function(doc, name, url) {
    invokeService = doc.createElement('script');
    invokeService.id = name;
    invokeService.setAttribute('type', 'text/javascript');
    invokeService.setAttribute('src', url);
    doc.body.appendChild(invokeService);
    
    return invokeService;
  },

  catchInvokeMiro : function(doc, event) {
    var element = event.target;

    if (element instanceof HTMLDivElement) {
      if (element.getAttribute('type') == "application/miroit-run-plugin") {
        var uri = element.getAttribute('uri');
        MiroIt.System.invokeMiro(uri);
      }
    }
  },

};
