MiroItService.GoogleReader = {

  title_href : null,
  chromeListener : null,
  entriesListener : null,

  parse : function(doc) {
    var chrome = doc.getElementById('chrome');

    var that = this;
    chrome.addEventListener('DOMSubtreeModified', chromeListener = function(event) {
      that.catchEntries(doc, event);
    }, false);

    var invokeCSS = doc.getElementById('invokeCSS');
    if (invokeCSS == null) {
      invokeCSS = doc.createElement('link');
      invokeCSS.id = 'invokeCSS';
      invokeCSS.setAttribute('rel', 'stylesheet');
      invokeCSS.setAttribute('type', 'text/css');
      invokeCSS.setAttribute('href', 'resource://miroit/services/googlereader.css');
      // invokeCSS.setAttribute('href',
      // 'chrome-extension://joenfdgfhhbclfbleelbagcmppfagfgk/services/googlereader.css');
      doc.head.appendChild(invokeCSS);
    }

  },

  // broken
  overrideEntries : function(doc) {
    var entries = doc.getElementById('entries');
    if (entries == null)
      return;

    alert(entries.insertBefore);
    entries._insertBefore = entries.insertBefore;
    entries.insertBefore = function(e1, e2) {
      alert('catch');
      entries._insertBefore(e1, e2);
    };
  },

  catchEntries : function(doc, event) {
    var element = event.target;

    if (element instanceof HTMLDivElement) {
      if (element.id == "entries") {
        var chrome = doc.getElementById('chrome');
        chrome.removeEventListener('DOMSubtreeModified', chromeListener, false);

        var that = this;
        element.addEventListener('DOMSubtreeModified', entriesListener = function(event) {
          that.catchEntriesChange(doc, event);
        }, false);
      }
    }
  },

  catchEntriesChange : function(doc, event) {
    this.mark(doc);
  },

  mark : function(doc) {
    // <span id="chrome-title">All items</span>
    var title = doc.getElementById('chrome-title');
    // div id="entries" class="single-source cards"
    var entries = doc.getElementById('entries');

    var title_href = null;
    if (title.children.length > 0)
      title_href = title.children[0].href;
    else
      title_href = title.value;

    var current = 0;

    if (this.title_href != title_href) {
      current = 0;
      this.title_href = title_href;
    }

    var max = entries.children.length - 1;

    for (; max > current; max--) {
      var entry = entries.children[max];
      // div id="scroll-filler"
      if (entry.id == 'scroll-filler')
        break;
    }

    for (; current < max; current++) {
      var entry = entries.children[current];
      if (entry.children.length > 0) {
        var card = entry.children[0];

        // <div class="card card-common">
        // here can be 'search-result', 'card card-0'
        if (card.className.indexOf('card card') != -1)
          this.addMiro(doc, card);
      }
    }

  },

  addMiro : function(doc, card) {
    // <div class="entry-actions">
    var icons = card.children[1].children[0];

    if (icons == null)
      return false;

    for ( var i = 0; i < icons.children.length; i++) {
      var icon = icons.children[i];
      if (icon.className.indexOf('miroit') != -1) {
        return false;
      }
    }

    {
      // <a class="entry-title-link">
      var title = card.children[0].children[0].children[0].children[1].children[0];

      // <div class="entry-body">
      var body = card.children[0].children[0].children[0].children[5];
      // <div class="audio-player-container player">
      var audio = null;

      if (body.children[0].children.length > 1)
        audio = body.children[0].children[1];

      // 1) check if audio box preset
      if (audio != null) {
        for ( var i = 0; i < audio.children.length; i++) {
          if (audio.children[i].className == 'view-enclosure-parent') {
            var href = audio.children[i].children[0].href;
            // check for supported audio formats
            if (this.checkFormat(href)) {
              this.addMiroIcon(doc, icons, href);
              return true; // exit
            }
          }
        }
      }

      // 2) check for native site support by miro
      if (this.checkSite(title.href)) {
        this.addMiroIcon(doc, icons, title.href);
        return true; // exit
      }
    }

    return false;
  },

  addMiroIcon : function(doc, icons, href) {
    miro = doc.createElement('span');
    miro.className = 'miroit';
    icons.insertBefore(miro, icons.children[2]);
    wbr = doc.createElement('wbr');
    icons.insertBefore(wbr, icons.children[3]);

    that = this;
    miro.onclick = function() {
      this.className = 'miroit visited';
      MiroItService.IPC.send(href);
    };
  },

  // return true if this url is native compatible to miro
  checkSite : function(url) {
    var href = [ 'youtube.com/watch' ];

    for ( var i = 0; i < href.length; i++) {
      if (url.indexOf(href[i]) != -1)
        return true;
    }

    return false;
  },

  // check file url if it native compatible to miro
  checkFormat : function(url) {
    url = url.toLowerCase();

    var href = [ '.jpg', '.png', '.gif', '.jpeg' ];

    for ( var i = 0; i < href.length; i++) {
      if (url.indexOf(href[i]) != -1)
        return false;
    }

    return true;
  }

};

MiroItService.GoogleReader.parse(document);