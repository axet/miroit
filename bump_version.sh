#!/bin/bash

VERSION=$1

if [ -z "$VERSION" ]; then
  echo "set versoin"
  exti 1
fi

DIR=$(git status --porcelain)

if [ ! -z "$DIR" ]; then
  echo "not on modified tree"
  exit 1
fi

sed -i "" "s/version>.*</version>${VERSION}</g" ./firefox/install.rdf

git commit -m "update version $VERSION" . || exit 1

git checkout master || exit 1

git merge --no-ff dev || exit 1

git tag "miroit-$VERSION" || exit 1

git checkout dev || exit 1

git push || exit 1

git push --tags || exit 1