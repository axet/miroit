# MERGED
Project is merged to the https://github.com/axet/mircle

# MiroIt!

MiroIt project allow you to download all media content from internet dirrectly
from browser.

Main idea of this project is to get missing Miro ability of having central
subscription place. Right now Miro miss this ability and you need otherwise
skip using Miro, use it on one machine or using tricky Dropbox hacks.

This probect does it better.

Right now I'm trying to fucusing to bring all RSS feeds to Google Reader and
add nice and easy way to run Miro on any content from Google Reader.

Check wiki pages for demo.

http://www.youtube.com/watch?v=K4SGe7QFbTc

# Wiki

https://github.com/axet/miroit/wiki

# Web page

http://axet.github.com/miroit/

# How to install

From official Mozilla store https://addons.mozilla.org/en-US/firefox/addon/miroit/

# How to install from source.

## Mac OS X
* mkdir ~/source
* cd ~/source
* git clone https://github.com/axet/miroit.git
* cd miroit
* make all ifirefox

Restart Firefox browser.
